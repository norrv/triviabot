package discord

import (
	"fmt"
	"strings"

	"github.com/bwmarrin/discordgo"
)

// Context is the context within a command is executed
type Context struct {
	Msg  *discordgo.Message
	Sess *discordgo.Session
	Args []string
}

// Send sends a message back on the channel the command was called from
func (c *Context) Send(args ...interface{}) (*discordgo.Message, error) {
	return c.Sess.ChannelMessageSend(c.Msg.ChannelID, fmt.Sprint(args...))
}

// SendBox wraps a message in a code block
func (c *Context) SendBox(args ...string) (*discordgo.Message, error) {
	return c.Send("```" + strings.Join(args, "") + "```")
}

// Sendf passes arguments through fmt.Sprintf
func (c *Context) Sendf(formatString string, args ...interface{}) (*discordgo.Message, error) {
	return c.Send(fmt.Sprintf(formatString, args...))
}

// Channel is a shorthand for grabbing the channel the command was sent on
func (c *Context) Channel() (*discordgo.Channel, error) {
	return c.Sess.State.Channel(c.Msg.ChannelID)
}

// MessageAuthor returns a Member object
func (c *Context) MessageAuthor() *discordgo.Member {
	auth, _ := c.Sess.GuildMember(c.Msg.GuildID, c.Msg.Author.ID)
	return auth
}

// MentionAuthor returns a string that can be used to mention the author
func (c *Context) MentionAuthor() string {
	return "<@" + c.Msg.Author.ID + ">"
}
