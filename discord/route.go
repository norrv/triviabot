package discord

import (
	"fmt"
	"strings"
)

// RouteNode represents part of a command
type RouteNode struct {
	Children    []*RouteNode
	Parent      *RouteNode
	Name        string
	Description string
	Usage       string
	Handler     HandlerFunc
	Middleware  []MiddlewareFunc
	MinArgs     int
	MaxArgs     int
}

// HandlerFunc does the actual command magic
type HandlerFunc func(ctx Context)

// MiddlewareFunc is for setup or permissions checking
type MiddlewareFunc func(ctx Context) bool

// Add adds a new child node, fails if name is already taken
func (rn *RouteNode) Add(newNode *RouteNode) {
	for _, r := range rn.Children {
		if r.Name == newNode.Name {
			return
		}
	}

	// add any middleware from the base node to the new node
	mdlwr := append(rn.Middleware, newNode.Middleware...)
	newNode.Middleware = mdlwr

	// ensure default expected values
	if newNode.Usage == "" {
		newNode.Usage = "%v"
	}
	if newNode.Description == "" {
		newNode.Description = fmt.Sprintf("%v is a command", newNode.Name)
	}

	rn.Children = append(rn.Children, newNode)
	newNode.Parent = rn
}

// Find finds the deepest node for the given args
// returns that node and the index of the first non matched string
// ex:
//    `>base_cmd sub_cmd arg1 arg2` would be called as:
//    rootNode.Find(">", "base_cmd", "sub_cmd", "arg1", "arg2")
//    and would return (sub_cmdRouteNode, 3)
func (rn *RouteNode) Find(args ...string) (*RouteNode, int) {
	var curr *RouteNode = rn
	var i int = 0
	if curr.Name != args[0] {
		return curr, i
	}
	i++

	for _, s := range args[1:] {
		temp := i
		for _, child := range curr.Children {
			if child.Name == s {
				curr = child
				i++
			}
		}
		if temp == i {
			break
		}
	}
	return curr, i
}

// GetFullHelpString returns the help string for a node and its children
func (rn *RouteNode) GetFullHelpString(prefix string) string {
	var msg []string = make([]string, 0)
	// check if we're at the root of the command tree
	if rn.Name == prefix {
		// print something about the bot
		msg = append(msg, "A trivia bot written in Go by norv\n")
	} else {
		// we should print this node's usage
		msg = append(msg, prefix+fmt.Sprintf(rn.Usage, rn.getCmdString(prefix))+"\n")
		// add the description
		msg = append(msg, fmt.Sprintf("\n%v\n", rn.Description))
	}
	if len(rn.Children) != 0 {
		msg = append(msg, "\nCommands:\n")
		var longestName int = 0
		for _, n := range rn.Children {
			if l := len(n.Name); l > longestName {
				longestName = l
			}
		}
		longestName = -1 * (longestName + 1)
		frmtString := fmt.Sprintf("  %%%dv %%v\n", longestName)

		for _, n := range rn.Children {
			msg = append(msg, fmt.Sprintf(frmtString, n.Name, n.Description))
		}

		msg = append(msg, fmt.Sprintf("\nType %vhelp command for more info on a command.\n", prefix))
	}

	return strings.Join(msg, "")
}

func (rn *RouteNode) getCmdString(prefix string) string {
	names := make([]string, 1)
	curr := rn
	names[0] = curr.Name
	for curr.Parent != nil && curr.Parent.Name != prefix {
		names = append(names, curr.Parent.Name)
		curr = curr.Parent
	}
	var n int = len(names)
	reversed := make([]string, n)
	for i, s := range names {
		reversed[n-i-1] = s
	}
	return strings.Join(reversed, " ")
}
