package trivia

import (
	"encoding/json"
	"fmt"
	"math/rand"
	"strings"
	"time"
)

// Question is a trivia object with one question and >= 1 answers
type Question struct {
	Question string
	Answers  []string
}

type triviaStatusEnum int

const (
	newQuestion triviaStatusEnum = iota
	stop
	waiting
	correct
)

// Settings are the settings for a trivia session
type Settings struct {
	MaxScore     int
	Delay        int
	Timeout      int
	RevealAnswer bool
	BotPlays     bool
}

// NewSettings creates a new TriviaSettings struct
func NewSettings(maxScore int, delay int, timeout int, revealAnswer bool, botPlays bool) Settings {
	return Settings{
		MaxScore:     maxScore,
		Delay:        delay,
		Timeout:      timeout,
		RevealAnswer: revealAnswer,
		BotPlays:     botPlays,
	}
}

// SettingsToJSON serializes a settings object
func (s Settings) SettingsToJSON() []byte {
	settingsJSON, _ := json.Marshal(s)
	return settingsJSON
}

// JSONToSettings deserializes json to a settings object
func JSONToSettings(data []byte) Settings {
	var s Settings
	json.Unmarshal(data, &s)
	return s
}

// A Guess consists of a user string and a guess string
type Guess struct {
	Author string
	Guess  string
}

// The UserInterface interface is the minimum set of funcs for a front end
type UserInterface interface {
	GetGuesses(sessID string) chan Guess
	GetResultsChannel(sessID string) chan map[string]int
	GetBotID() string
	SendMessage(sessID string, msg string)
}

// A Session holds all the details for a single session
type Session struct {
	UserInterface  UserInterface
	ID             string
	revealMessages [3]string
	failMessages   [4]string
	currentTrivia  Question
	triviaList     []Question
	scores         map[string]int // mapping of users to their scores; might want to push to user interface
	status         triviaStatusEnum
	timeout        *time.Timer
	count          int
	settings       Settings
}

// NewSession creates a new trivia session object
func NewSession(userInterface UserInterface, ID string, triviaList []Question, settings Settings) Session {
	rand.Seed(time.Now().UTC().UnixNano())
	var sess Session = Session{
		UserInterface: userInterface,
		ID:            ID,
		revealMessages: [3]string{"I know this one! %v!",
			"Easy: %v.",
			"Oh really? It's %v of course."},
		failMessages: [4]string{"To the next one I guess...",
			"Moving on...",
			"I'm sure you'll know the answer of the next one.",
			"Next one."},
		triviaList: triviaList,
		scores:     make(map[string]int),
		status:     newQuestion,
		timeout:    time.NewTimer(time.Duration(settings.Timeout) * time.Second),
		count:      0,
		settings:   settings,
	}
	return sess
}

// StartSession starts a trivia session
func (sess *Session) StartSession() {
	for sess.status != stop {
		// check if anyone has won yet
		for _, score := range sess.scores {
			if score >= sess.settings.MaxScore {
				sess.endGame()
				return
			}
		}

		// check if there are questions left to ask
		if len(sess.triviaList) == 0 {
			sess.endGame()
			return
		}

		var ch chan string = make(chan string)

		go sess.newQuestion(ch)

		select {
		case user, ok := <-ch:
			if ok {
				sess.scores[user]++
			} else {
				sess.endGame()
				return
			}
		case <-sess.timeout.C:
			sess.UserInterface.SendMessage(sess.ID, "Hello? Well, I guess I'll stop then.")
			sess.endGame()
			return
		}
	}
}

// NewQuestion will kick off a trivia session and run, returns map of users to scores
//   NOTE: may want to push results into a channel instead of returning
func (sess *Session) newQuestion(ch chan string) {
	// set the current question
	var currIdx int = rand.Intn(len(sess.triviaList))
	sess.currentTrivia = sess.triviaList[currIdx]
	sess.triviaList[currIdx] = sess.triviaList[len(sess.triviaList)-1]
	sess.triviaList = sess.triviaList[:len(sess.triviaList)-1]
	sess.count++

	// start timer
	var timer *time.Timer = time.NewTimer(time.Duration(sess.settings.Delay) * time.Second)
	var guesses chan Guess = sess.UserInterface.GetGuesses(sess.ID)
	clearChannel(guesses)
	sess.status = waiting

	var q string = fmt.Sprintf("**Question number %v!**\n\n%v", sess.count, sess.currentTrivia.Question)
	sess.UserInterface.SendMessage(sess.ID, q)

	for sess.status == waiting {
		select {
		case guess, ok := <-guesses:
			if ok {
				if sess.checkAnswer(guess) {
					sess.status = correct
					var msg string = fmt.Sprintf("You got it %v! **+1** to you!", guess.Author)
					sess.UserInterface.SendMessage(sess.ID, msg)
					ch <- guess.Author
				}
			} else {
				sess.endGame()
			}
		case <-timer.C:
			// timed out
			// build our message string
			var msg string
			if sess.settings.RevealAnswer {
				msg = fmt.Sprintf(sess.revealMessages[rand.Intn(len(sess.revealMessages))], strings.Join(sess.currentTrivia.Answers, ", "))
			} else {
				msg = sess.failMessages[rand.Intn(len(sess.failMessages))]
			}
			if sess.settings.BotPlays {
				msg = msg + " **+1** for me!"
			}

			// send message string
			sess.UserInterface.SendMessage(sess.ID, msg)
			sess.status = newQuestion
			ch <- sess.UserInterface.GetBotID()
		}
	}
}

func (sess *Session) checkAnswer(guess Guess) bool {
	var lowerGuess string = strings.ToLower(guess.Guess)
	sess.timeout.Reset(time.Duration(sess.settings.Timeout) * time.Second)
	for _, ans := range sess.currentTrivia.Answers {
		if strings.ToLower(ans) == lowerGuess {
			return true
		}
	}
	return false
}

func (sess *Session) endGame() {
	sess.status = stop
	sess.timeout.Stop()
	ch := sess.UserInterface.GetResultsChannel(sess.ID)
	ch <- sess.scores
	close(ch)
}

func clearChannel(ch chan Guess) {
	l := len(ch)
	for i := 0; i < l; i++ {
		<-ch
	}
}
