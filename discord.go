package main

import (
	"bufio"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"net/http"
	"os"
	"os/signal"
	"sort"
	"strconv"
	"strings"
	"syscall"
	"trivia/discord"
	"trivia/trivia"

	"github.com/bwmarrin/discordgo"
)

// Variables for command line parameters and global data (settings)
var (
	Token            string
	Prefix           string
	Settings         trivia.Settings
	SettingsFileName string
	TriviaListPath   string
	Root             discord.RouteNode
	AuthorizedUsers  arrayFlag
	GuildRoles       guildRoles
	Sessions         map[string]trivia.Session   // map of channel IDs to sessions
	Interfaces       map[string]discordInterface // map of session IDs to interfaces
	Lists            []string
	BotOwner         string
	sigChan          chan os.Signal
)

type guildRoles struct {
	guildID string
	loaded  bool
	roles   discordgo.Roles
	roleMap map[string]*discordgo.Role
}

func (gr *guildRoles) GetRoles(id string, s *discordgo.Session) guildRoles {
	if gr.loaded && gr.guildID == id {
		return *gr
	}
	gr.Load(id, s)
	return *gr
}

func (gr *guildRoles) Load(id string, s *discordgo.Session) {
	gr.guildID = id
	gr.roles, _ = s.GuildRoles(gr.guildID)
	gr.roleMap = make(map[string]*discordgo.Role)
	for _, r := range gr.roles {
		gr.roleMap[r.ID] = r
	}
	gr.loaded = true
}

type discordInterface struct {
	guesses chan trivia.Guess
	results chan map[string]int
	botName string
	ctx     discord.Context
}

func (di *discordInterface) GetGuesses(sessID string) chan trivia.Guess {
	return di.guesses
}

func (di *discordInterface) GetResultsChannel(sessID string) chan map[string]int {
	return di.results
}

func (di *discordInterface) GetBotID() string {
	return di.botName
}

func (di *discordInterface) SendMessage(sessID string, msg string) {
	di.ctx.Send(msg)
}

type arrayFlag []string

func (af *arrayFlag) String() string {
	return strings.Join(*af, ", ")
}

func (af *arrayFlag) Set(value string) error {
	*af = strings.Split(value, ",")
	return nil
}

func init() {
	// parse command line
	flag.StringVar(&Token, "t", "", "Bot Token")
	flag.StringVar(&Prefix, "p", ">", "Bot's prefix")
	flag.StringVar(&SettingsFileName, "s", "settings.json", "Settings file")
	flag.StringVar(&BotOwner, "o", "280833495700471817", "Bot owner's id")
	flag.StringVar(&TriviaListPath, "l", "./trivia_lists", "The path to the directroy with the trivia lists")
	flag.Var(&AuthorizedUsers, "au", "Normal users allowed to use triviaload")
	flag.Parse()

	log.Println("Settings at boot")
	log.Println("-------------------------")
	log.Println("Token: ", Token)
	log.Println("Prefix: ", Prefix)
	log.Println("Settings file name: ", SettingsFileName)
	log.Println("Bot owner id: ", BotOwner)
	log.Println("Trivia list path: ", TriviaListPath)
	log.Println("Authorized users: ", AuthorizedUsers)

	// load settings
	data, err := ioutil.ReadFile(SettingsFileName)

	if err != nil {
		log.Fatal("Could not load settings file", err)
	}

	Settings = trivia.JSONToSettings(data)
	log.Printf("Settings loaded: %v\n", Settings)

	// pretty sure this is the default but lets be sure
	GuildRoles.loaded = false

	// set of commands
	Root = discord.RouteNode{
		Name:        Prefix,
		Description: "The prefix",
		Handler:     prefixHandler,
	}
	Root.Add(&discord.RouteNode{
		Name:        "hello",
		Description: "Says hello",
		Handler:     helloHandler,
	})
	Root.Add(&discord.RouteNode{
		Name:        "kill",
		Description: "kills the bot (owner only)",
		Handler:     killHandler,
		Middleware:  []discord.MiddlewareFunc{isOwner},
	})
	Root.Add(&discord.RouteNode{
		Name:        "debug",
		Description: "general debugging (owner only)",
		Handler:     debugHandler,
		Middleware:  []discord.MiddlewareFunc{isOwner, disabled},
	})

	var settingNode discord.RouteNode = discord.RouteNode{
		Name:        "triviaset",
		Description: "Change trivia settings",
		Usage:       "%v",
		Handler:     triviasetHandler,
		Middleware:  []discord.MiddlewareFunc{hasModPermissions},
	}
	settingNode.Add(&discord.RouteNode{
		Name:        "maxscore",
		Description: "Points required to win",
		Usage:       "%v <score>",
		Handler:     maxscoreHandler,
		Middleware:  []discord.MiddlewareFunc{hasAdminPermissions},
		MinArgs:     1,
		MaxArgs:     1,
	})
	settingNode.Add(&discord.RouteNode{
		Name:        "timelimit",
		Description: "Maximum seconds to answer",
		Usage:       "%v <seconds>",
		Handler:     timelimitHandler,
		Middleware:  []discord.MiddlewareFunc{hasAdminPermissions},
		MinArgs:     1,
		MaxArgs:     1,
	})
	settingNode.Add(&discord.RouteNode{
		Name:        "botplays",
		Description: "Bot gains points",
		Usage:       "%v [true|false]",
		Handler:     botplaysHandler,
		Middleware:  []discord.MiddlewareFunc{hasAdminPermissions},
		MaxArgs:     1,
	})
	settingNode.Add(&discord.RouteNode{
		Name:        "revealanswer",
		Description: "Reveals answer to questions on timeout",
		Usage:       "%v [true|false]",
		Handler:     revealanswerHandler,
		Middleware:  []discord.MiddlewareFunc{hasAdminPermissions},
		MaxArgs:     1,
	})
	Root.Add(&settingNode)

	var triviaNode discord.RouteNode = discord.RouteNode{
		Name:        "trivia",
		Description: "Start a trivia session with the specified list",
		Usage:       "%v <list_name>",
		Handler:     triviaHandler,
		MinArgs:     1,
		MaxArgs:     1,
	}
	triviaNode.Add(&discord.RouteNode{
		Name:        "list",
		Description: "Shows available trivia lists",
		Usage:       "%v",
		Handler:     trivialistHandler,
	})
	triviaNode.Add(&discord.RouteNode{
		Name:        "stop",
		Description: "Stops an ongoing trivia session",
		Usage:       "%v",
		Handler:     triviastopHandler,
		Middleware:  []discord.MiddlewareFunc{hasTriviaStopPermissions},
	})
	Root.Add(&triviaNode)

	Root.Add(&discord.RouteNode{
		Name:        "triviaload",
		Description: "loads trivia file from bitbucket",
		Usage:       "%v <list_name>",
		Handler:     trivialoadHandler,
		Middleware:  []discord.MiddlewareFunc{hasTriviaLoadPermissions},
		MinArgs:     1,
		MaxArgs:     1,
	})

	Root.Add(&discord.RouteNode{
		Name:        "help",
		Description: "Shows this message",
		Usage:       "%v [commands...]",
		Handler:     helpHandler,
		MinArgs:     0,
		MaxArgs:     math.MaxInt64,
	})

	// initialize session map
	Sessions = make(map[string]trivia.Session)
	Interfaces = make(map[string]discordInterface)

	// load lists
	Lists = loadLists()
	log.Println("Lists: ", Lists)
}

func main() {
	// Create a new discord session with token
	dg, err := discordgo.New("Bot " + Token)

	if err != nil {
		log.Fatal("error creating session", err)
	}

	defer dg.Close()

	// Register onMessage as the callback func for all messages
	dg.AddHandler(onMessage)

	// one discord connection
	err = dg.Open()
	if err != nil {
		fmt.Println("error opening connection", err)
	}

	// Wait here to die
	log.Println("bot running, use ctrl-c or kill to quit")
	sigChan = make(chan os.Signal, 1)
	signal.Notify(sigChan, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sigChan
}

func onMessage(s *discordgo.Session, m *discordgo.MessageCreate) {
	// ignore self
	if m.Author.ID == s.State.User.ID {
		return
	}

	if len(m.Content) > 0 && string(m.Content[0]) == Prefix {
		var args []string = make([]string, 1)
		args[0] = string(m.Content[0])
		args = append(args, strings.Split(string(m.Content[1:]), " ")...)

		// handle some aliases
		if args[1] == "t" {
			args[1] = "trivia"
		} else if args[1] == "tl" {
			var newArgs []string = []string{args[0], "trivia", "list"}
			args = append(newArgs, args[2:]...)
		} else if args[1] == "ts" {
			var newArgs []string = []string{args[0], "trivia", "stop"}
			args = append(newArgs, args[2:]...)
		} else if args[1] == "twtp" {
			var newArgs []string = []string{args[0], "trivia", "whosthatpokemon"}
			args = append(newArgs, args[2:]...)
		} else if m.Content == fmt.Sprintf("%vhelp", Prefix) {
			// bare help command; print usage for all commands
			s.ChannelMessageSend(m.ChannelID, "```\n"+Root.GetFullHelpString(Prefix)+"```")
			return
		}

		var node, argNo = Root.Find(args...)

		if node.MinArgs > node.MaxArgs {
			node.MaxArgs = node.MinArgs
		}

		if l := len(args[argNo:]); l < node.MinArgs || l > node.MaxArgs {
			// wrong number of arguments
			var newArgs []string = []string{args[0], "help"}
			args = append(newArgs, args[1:]...)
			node, argNo = Root.Find(args...)
		}

		var ctx discord.Context = discord.Context{
			Msg:  m.Message,
			Sess: s,
			Args: args[argNo:],
		}
		for _, mdlwr := range node.Middleware {
			if !mdlwr(ctx) {
				ctx.Send(fmt.Sprintf("%v: you do not have permission to run that command", ctx.MentionAuthor()))
				return
			}
		}
		node.Handler(ctx)
	}

	if sess, exists := Sessions[m.ChannelID]; exists {
		defer recoverAll()
		g := trivia.Guess{
			Author: m.Author.String(),
			Guess:  strings.Replace(m.Content, "\r\n", "", -1),
		}
		Interfaces[sess.ID].guesses <- g
	}
}

// helpers
func getMemberRoles(guildID string, s *discordgo.Session, m discordgo.Member) discordgo.Roles {
	var gr guildRoles = GuildRoles.GetRoles(guildID, s)
	var authorRoleIDs []string = m.Roles
	var roles discordgo.Roles
	for _, ur := range authorRoleIDs {
		roles = append(roles, gr.roleMap[ur])
	}
	return roles
}

func save(s trivia.Settings) error {
	var f, err = os.Create(SettingsFileName)
	if err != nil {
		log.Println("failed to save settings", err)
		return err
	}
	defer f.Close()

	_, err = f.Write(s.SettingsToJSON())
	return err
}

func loadLists() []string {
	f, err := os.Open(TriviaListPath)
	if err != nil {
		log.Fatal("unable to open trivia list directory")
	}
	defer f.Close()

	list, _ := f.Readdirnames(0)
	for i, name := range list {
		list[i] = strings.Split(name, ".")[0]
	}
	sort.Strings(list)
	return list
}

func loadSingleList(list string) []trivia.Question {
	var fname string = fmt.Sprintf("%v/%v.txt", TriviaListPath, list)

	var file *os.File
	var err error
	file, err = os.Open(fname)
	if err != nil {
		log.Fatalf("could not find file %v", fname)
	}

	// make sure to close the file when we're done with it
	defer file.Close()

	// read through the file and parse into list of trivia.Question objects
	var trivias []trivia.Question = make([]trivia.Question, 0)
	var scanner *bufio.Scanner = bufio.NewScanner(file)
	for scanner.Scan() {
		var line []string = strings.Split(scanner.Text(), "`")
		trivias = append(trivias, trivia.Question{Question: line[0], Answers: line[1:]})
	}
	return trivias
}

func saveSingleList(list string, listContent []byte) error {
	var fname string = fmt.Sprintf("%v/%v.txt", TriviaListPath, list)
	var f, err = os.Create(fname)
	if err != nil {
		log.Println("failed to save list", err)
		return err
	}
	defer f.Close()

	_, err = f.Write(listContent)
	return err
}

func getListURL(list string, url string) (bool, string) {
	resp, err := http.Get(url)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()
	var body map[string]interface{}
	json.NewDecoder(resp.Body).Decode(&body)
	t := body["values"].([]interface{})
	for _, v := range t {
		if v.(map[string]interface{})["path"].(string) == list {
			listURL := v.(map[string]interface{})["links"].(map[string]interface{})["self"].(map[string]interface{})["href"].(string)
			return true, listURL
		}
	}
	if nextURL, exists := body["next"]; exists {
		return false, nextURL.(string)
	}
	return false, ""
}

func recoverAll() {
	if r := recover(); r != nil {
		log.Println("recovered from ", r)
	}
}

// MiddlewareFuncs
func hasAdminPermissions(ctx discord.Context) bool {
	// bot owner automatically passes
	if isOwner(ctx) {
		return true
	}
	var roles discordgo.Roles = getMemberRoles(ctx.Msg.GuildID, ctx.Sess, *ctx.MessageAuthor())
	for _, r := range roles {
		if (r.Permissions & discordgo.PermissionAdministrator) > 0 {
			return true
		}
	}
	log.Printf("member %v failed to pass hasAdminPermissions check", ctx.Msg.Member.Nick)
	return false
}

func hasModPermissions(ctx discord.Context) bool {
	// owner and admin automatically pass
	if isOwner(ctx) || hasAdminPermissions(ctx) {
		return true
	}
	var roles discordgo.Roles = getMemberRoles(ctx.Msg.GuildID, ctx.Sess, *ctx.MessageAuthor())
	for _, r := range roles {
		if (r.Permissions & discordgo.PermissionKickMembers) > 0 {
			return true
		}
	}
	log.Printf("member %v failed to pass hasModPermissions check", ctx.Msg.Member.Nick)
	return false
}

func isOwner(ctx discord.Context) bool {
	if ctx.Msg.Author.ID == BotOwner {
		return true
	}
	log.Printf("member %v failed to pass isOwner check", ctx.Msg.Member.Nick)
	return false
}

func disabled(ctx discord.Context) bool {
	log.Printf("member %v tried to run disabled command", ctx.Msg.Member.Nick)
	return false
}

func hasTriviaStopPermissions(ctx discord.Context) bool {
	// get trivia session for this guild/channel
	sess, exists := Sessions[ctx.Msg.ChannelID]
	if !exists {
		ctx.Send("No trivia session in this channel")
		return true
	}

	// check if member has elevated permissions
	var isAuthorized = hasAdminPermissions(ctx) || hasModPermissions(ctx) || isOwner(ctx)

	// if they have levated permissions or started the session, then they can end it
	return isAuthorized || Interfaces[sess.ID].ctx.Msg.Author.ID == ctx.Msg.Author.ID
}

func hasTriviaLoadPermissions(ctx discord.Context) bool {
	var isAuthorized = hasAdminPermissions(ctx) || hasModPermissions(ctx) || isOwner(ctx)
	var isSpecial bool = false
	for _, id := range AuthorizedUsers {
		if id == ctx.Msg.Author.ID {
			isSpecial = true
			break
		}
	}
	return isAuthorized || isSpecial
}

// HandlerFuncs
func prefixHandler(ctx discord.Context) {
	ctx.Send(fmt.Sprintf("Invalid command: ```%v```", strings.Join(ctx.Args, " ")))
}

func helloHandler(ctx discord.Context) {
	ctx.Send(fmt.Sprintf("Hello, %v", ctx.Msg.Author.Username))
}

func killHandler(ctx discord.Context) {
	defer recoverAll()
	ctx.Send("Goodbye")
	sigChan <- syscall.SIGTERM
}

func debugHandler(ctx discord.Context) {
	var roles = GuildRoles.GetRoles(ctx.Msg.GuildID, ctx.Sess)
	var perms map[string]int = make(map[string]int)
	for _, r := range roles.roles {
		perms[r.Name] = r.Permissions
	}
	var msg string = "Perms:\n\n"
	for k, v := range perms {
		msg = msg + fmt.Sprintf("%v\t%v\n", k, v&discordgo.PermissionKickMembers)
	}
	ctx.Send(fmt.Sprintf("```%v```", msg))
}

func triviasetHandler(ctx discord.Context) {
	if len(ctx.Args) != 0 {
		ctx.Send(fmt.Sprintf("Arguments %v to triviaset not valid, please see ``%vhelp triviaset`` for help", ctx.Args, Prefix))
	}
	ctx.SendBox("c\n",
		"Current settings:\n\n",
		fmt.Sprintf("Bot gains points: %v\n", Settings.BotPlays),
		fmt.Sprintf("Seconds to answer: %v\n", Settings.Delay),
		fmt.Sprintf("Points to win: %v\n", Settings.MaxScore),
		fmt.Sprintf("Reveal answer on timeout: %v\n", Settings.RevealAnswer),
		fmt.Sprintf("\nSee %vhelp triviaset to edit the settings", Prefix))
}

func maxscoreHandler(ctx discord.Context) {
	if len(ctx.Args) == 1 {
		var newVal, err = strconv.ParseInt(ctx.Args[0], 10, 32)
		if err != nil {
			msg := fmt.Sprintf("Error parsing argument %v", ctx.Args[0])
			ctx.Send(msg)
			log.Println(msg, err)
			return
		}
		Settings.MaxScore = int(newVal)
		err = save(Settings)
		if err != nil {
			msg := "error saving settings to disk"
			ctx.Send(msg)
			log.Println(msg, err)
			return
		}
		ctx.Sendf("New max score %v saved", Settings.MaxScore)
	} else if len(ctx.Args) == 0 {
		ctx.Send("Too few arguments")
	} else {
		ctx.Send("Too few arguments")
	}
}

func timelimitHandler(ctx discord.Context) {
	if len(ctx.Args) == 1 {
		var newVal, err = strconv.ParseInt(ctx.Args[0], 10, 32)
		if err != nil {
			msg := fmt.Sprintf("Error parsing argument %v", ctx.Args[0])
			ctx.Send(msg)
			log.Println(msg, err)
			return
		}
		Settings.Delay = int(newVal)
		err = save(Settings)
		if err != nil {
			msg := "error saving settings to disk"
			ctx.Send(msg)
			log.Println(msg, err)
			return
		}
		ctx.Sendf("New time limit %v saved", Settings.Delay)
	} else if len(ctx.Args) == 0 {
		ctx.Send("Too few arguments")
	} else {
		ctx.Send("Too few arguments")
	}
}

func botplaysHandler(ctx discord.Context) {
	if len(ctx.Args) == 1 {
		var newVal, err = strconv.ParseBool(ctx.Args[0])
		if err != nil {
			msg := fmt.Sprintf("Error parsing argument %v", ctx.Args[0])
			ctx.Send(msg)
			log.Println(msg, err)
			return
		}
		Settings.BotPlays = newVal
		err = save(Settings)
		if err != nil {
			msg := "error saving settings to disk"
			ctx.Send(msg)
			log.Println(msg, err)
			return
		}
		ctx.Sendf("New bot plays %v value saved", Settings.BotPlays)
	} else if len(ctx.Args) == 0 {
		Settings.BotPlays = !Settings.BotPlays
		if err := save(Settings); err != nil {
			msg := "error saving settings to disk"
			ctx.Send(msg)
			log.Println(msg, err)
			return
		}
		ctx.Sendf("New bot plays %v value saved", Settings.BotPlays)
	} else {
		ctx.Send("Too few arguments")
	}
}

func revealanswerHandler(ctx discord.Context) {
	if len(ctx.Args) == 1 {
		var newVal, err = strconv.ParseBool(ctx.Args[0])
		if err != nil {
			msg := fmt.Sprintf("Error parsing argument %v", ctx.Args[0])
			ctx.Send(msg)
			log.Println(msg, err)
			return
		}
		Settings.RevealAnswer = newVal
		err = save(Settings)
		if err != nil {
			msg := "error saving settings to disk"
			ctx.Send(msg)
			log.Println(msg, err)
			return
		}
		ctx.Sendf("New reveal answer %v value saved", Settings.RevealAnswer)
	} else if len(ctx.Args) == 0 {
		Settings.RevealAnswer = !Settings.RevealAnswer
		if err := save(Settings); err != nil {
			msg := "error saving settings to disk"
			ctx.Send(msg)
			log.Println(msg, err)
			return
		}
		ctx.Sendf("New reveal answer %v value saved", Settings.RevealAnswer)
	} else {
		ctx.Send("Too few arguments")
	}
}

func triviaHandler(ctx discord.Context) {
	// check if a session already exists for this channel
	_, exists := Sessions[ctx.Msg.ChannelID]
	if exists {
		ctx.Send("A trivia session is already ongoing for this channel")
		return
	}

	// if not, check if that list exists
	var list string = ctx.Args[0]
	for _, l := range Lists {
		if list == l {
			var trivias []trivia.Question = loadSingleList(list)
			itrfc := &discordInterface{make(chan trivia.Guess, 30), make(chan map[string]int), ctx.Sess.State.User.String(), ctx}
			ts := trivia.NewSession(itrfc, ctx.Msg.ChannelID, trivias, Settings)

			Sessions[ts.ID] = ts
			Interfaces[ts.ID] = *itrfc
			go ts.StartSession()

			defer recoverAll()
			results, _ := <-itrfc.results
			var msg string = "diff\nResults:\n\n"
			var maxscore int = 0
			var longestName int = 0
			for k, v := range results {
				if v > maxscore {
					maxscore = v
				}
				if l := len(k); l > longestName {
					longestName = l
				}
			}

			longestName = -1 * (longestName + 1)
			frmtString := fmt.Sprintf("%%v %%%dv %%v\n", longestName)

			for k, v := range results {
				var sym string = "-"
				if v == maxscore {
					sym = "+"
				}
				msg = msg + fmt.Sprintf(frmtString, sym, k, v)
			}
			ctx.SendBox(msg)
			delete(Interfaces, ts.ID)
			delete(Sessions, ts.ID)
			return
		}
	}
	msg := fmt.Sprintf("Could not find list %v", list)
	ctx.Send(msg)
	log.Println(msg)
}

func trivialistHandler(ctx discord.Context) {
	var lists []string = make([]string, len(Lists))
	for i, l := range Lists {
		lists[i] = l + "\n"
	}
	ctx.SendBox(lists...)
}

func triviastopHandler(ctx discord.Context) {
	// stop the session in this channel
	if di, exists := Interfaces[ctx.Msg.ChannelID]; exists {
		close(di.guesses)
	}
}

func helpHandler(ctx discord.Context) {
	args := []string{Prefix}
	args = append(args, ctx.Args...)
	var node, _ = Root.Find(args...)
	ctx.SendBox(node.GetFullHelpString(Prefix))
}

func trivialoadHandler(ctx discord.Context) {
	var list = ctx.Args[0]
	for _, l := range Lists {
		if list == l {
			list = list + ".txt"
			found, url := getListURL(list, "https://api.bitbucket.org/2.0/repositories/norrv/trivia_lists/src")
			for !found && url != "" {
				found, url = getListURL(list, url)
			}
			if found {
				resp, err := http.Get(url)
				if err != nil {
					log.Fatal(err)
				}
				defer resp.Body.Close()
				body, _ := ioutil.ReadAll(resp.Body)
				err = saveSingleList(ctx.Args[0], body)
				if err != nil {
					msg := fmt.Sprintf("Failed to save list %v", list)
					ctx.Send(msg)
					log.Println(msg)
					return
				}
				Lists = loadLists()
				ctx.Send("trivia file loaded (probably)")
			}
		}
	}
}
