package cmdline

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strings"
	"trivia/trivia"
)

type cmdLineInterface struct {
	guesses chan trivia.Guess
	results chan map[string]int
	botID   string
}

func (cli *cmdLineInterface) GetGuesses(sessID string) chan trivia.Guess {
	return cli.guesses
}

func (cli *cmdLineInterface) GetResultsChannel(sessID string) chan map[string]int {
	return cli.results
}

func (cli *cmdLineInterface) GetBotID() string {
	return cli.botID
}

func (cli *cmdLineInterface) SendMessage(sessID string, msg string) {
	fmt.Println(msg)
}

func (cli *cmdLineInterface) GetInput() {
	reader := bufio.NewReader(os.Stdin)
	fmt.Println("[q] to quit")

	for {
		text, err := reader.ReadString('\n')
		if err == nil {
			g := trivia.Guess{
				Author: "user",
				Guess:  strings.Replace(text, "\r\n", "", -1),
			}
			if g.Guess == "q" {
				close(cli.guesses)
				break
			}
			cli.guesses <- g
		}
	}
}

func main() {
	var listName string = "uscapitals"
	var fname string = fmt.Sprintf("./trivia_lists/%v.txt", listName)

	var file *os.File
	var err error
	file, err = os.Open(fname)
	if err != nil {
		log.Fatalf("could not find file %v", fname)
	}

	// make sure to close the file when we're done with it
	defer file.Close()

	// read through the file and parse into list of trivia.Question objects
	var trivias []trivia.Question = make([]trivia.Question, 0)
	var scanner *bufio.Scanner = bufio.NewScanner(file)
	for scanner.Scan() {
		var line []string = strings.Split(scanner.Text(), "`")
		fmt.Printf("Question: %v; Answers: %v\n", line[0], line[1:])
		trivias = append(trivias, trivia.Question{Question: line[0], Answers: line[1:]})
	}

	// fmt.Println(trivias)

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	// create cli interface
	cli := &cmdLineInterface{make(chan trivia.Guess, 10), make(chan map[string]int), "007"}
	settings := trivia.NewSettings(10, 10, 20, true, true)

	// create a trivia session
	ts := trivia.NewSession(cli, "1", trivias, settings)

	// start the session
	go ts.StartSession()
	go cli.GetInput()

	results, _ := <-cli.results
	var msg string = "Results:\n\n"
	for k, v := range results {
		msg = msg + fmt.Sprintf("+ %v\t%v\n", k, v)
	}
	fmt.Println(msg)
}
